package net.indyvision.nexthackathon;

import android.widget.RelativeLayout;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import net.indyvision.nexthackathon.pojo.interfaces.mvc.OnChangeListener;

public class MvcLayout extends RelativeLayout implements OnChangeListener<MvcModel> {

    private final String TAG = "MvcLayout";

    private MvcModel model;
    private ViewListener viewListener;

    public interface ViewListener {
    }

    public MvcLayout(Context context) {
        super(context);
    }

    public MvcLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MvcLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
        initToolbar();
    }

    private void initLayout() {
    }

    private void initToolbar() {
    }

    private void updateView() {
    }

    @Override
    public void onChange() {
        updateView();
    }

    public MvcModel getModel() {
        return model;
    }

    public void setModel(MvcModel model) {
        this.model = model;
        this.model.addListener(this);
        updateView();
    }

    public ViewListener getViewListener() {
        return viewListener;
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

}
