package net.indyvision.nexthackathon;

import android.util.Log;

import net.indyvision.nexthackathon.pojo.interfaces.mvc.SimpleObservable;

public class MvcModel extends SimpleObservable<MvcModel> {

    private final String TAG = "MvcModel";

    public void update(boolean... notifyObservers) {
        if (notifyObservers.length > 0 && notifyObservers[0]) {
            notifyObservers();
        }
    }
}
