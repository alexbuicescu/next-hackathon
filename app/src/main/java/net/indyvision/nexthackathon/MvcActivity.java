package net.indyvision.nexthackathon;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MvcActivity extends AppCompatActivity {

    private final String TAG = "MvcActivity";

    private Activity activity;

    private MvcLayout layout;
    private MvcModel model;
    private MvcLayout.ViewListener viewListener = new MvcLayout.ViewListener() {
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = MvcActivity.this;

        initModel();
        initLayout();

        setContentView(layout);
    }

    private void initModel() {
        model = new MvcModel();
    }

    private void initLayout() {
        layout = (MvcLayout) View.inflate(activity, R.layout.activity_mvc, null);
        layout.setModel(model);
        layout.setViewListener(viewListener);
    }
}
