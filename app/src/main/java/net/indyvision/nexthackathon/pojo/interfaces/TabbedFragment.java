package net.indyvision.nexthackathon.pojo.interfaces;

import net.indyvision.metronome.activities.MetronomeRootListener;

/**
 * Created by alexbuicescu on 19.08.2015.
 */
public interface TabbedFragment extends MetronomeRootListener {

    boolean consumeOnBackPressed();
    void manageOrientationChange(boolean isInPortrait);
    void onWindowFocusChanged(boolean hasFocus);
    void manageSelection();
    void manageUnselection();
}
